#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void monosimple()
{
    // ----------------------- Initialisation -----------------------

    double pi,j;
    pi = 3.14159265;
    j = 6; // densité de courant maximal normalisée en A.mm^-2
    printf("\n\n\n");
    printf("Vous êtes dans le programme monophasé simple.\n");
    printf("¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ \n\n");

    // ----------------------- Récupération des variables  -----------------------

    double Puiss,Bmax,Se;
    printf("Général :\n");
    printf("Puissance (VA) = ");
    scanf("%lf",&Puiss);
    printf("Champ magnétique maximal admissible (Tesla) = ");
    scanf("%lf",&Bmax);
    printf("Surface interne de la bobine (m²) = ");
    scanf("%lf",&Se);
    printf("\n\n");

    double Ve,Fe;
    printf("Entrée :\n");
    printf("Tension Veff (Volt) = ");
    scanf("%lf",&Ve);
    printf("Fréquence (Hertz) = ");
    scanf("%lf",&Fe);
    printf("\n\n");

    double Vs;
    printf("Sortie :\n");
    printf("Tension Veff (Volt) = ");
    scanf("%lf",&Vs);
    printf("\n\n");

    // ----------------------- Réalisation des calculs -----------------------

    double Ns,Ne;
    Ne = (sqrt(2)*Ve)/(Se*Fe*2*pi*Bmax);
    Ns = Ne * (Vs/Ve);

    double Is,ScuE;
    Is = Puiss/Vs;
    ScuE = Is/j;

    // ----------------------- Affichages des résultats -----------------------

    printf("Il faut donc %lf spires en entrée et %lf en sortie.\n",Ne,Ns);
    printf("De là, on obtient un diamètre du fil de sortie de %lf mm²\n",ScuE);

}

int main(int argc, char *argv[])
{

    // Variable

    int choix = 0;

    // Initialisation

    printf("Ouverture du programme de calcul du bobinage d'un transformateur\n");
    printf("¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨\n\n");

    while(1==1)
    {
        // Proposition des choix
        printf("Choix du programme : \n");
        printf("¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨ \n\n");
        printf("Monophasé : \n");
        printf("(1) Simple\n");
        printf("(2) Détaillé (indisponible)\n\n");
        printf("Triphasé : \n");
        printf("(3) Simple (indisponible)\n");
        printf("(4) Détaillé (indisponible)\n\n");
        printf("Choix :");
        scanf("%d",&choix);

        // Renvoie vers la fonction adéquate
        switch (choix)
        {
            case 1:
                monosimple();
                break;
            case 2:
                printf("\n\nChoix 2 indisponible\n\n");
                break;
            case 3:
                printf("\n\nChoix 3 indisponible\n\n");
                break;
            case 4:
                printf("\n\nChoix 4 indisponible\n\n");
                break;
            default:
                printf("Numéro incorrect");
                break;
        }
    }

    return 0;
}
